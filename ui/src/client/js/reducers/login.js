import ActionTypes from '../constants/ActionTypes';

export const initialState = {
    lastUpdated: null,
    isFetching: false,
    hasFetched: false,
    hasError: false,
    error: null,
    result: {},
    response: {}
};

function login(state = initialState, { type, payload, response, meta }) {
	switch(type) {
		case ActionTypes.LOGIN_SUCCESS:
			return Object.assign({}, state, {
                    result: Object.assign({}, state.result, payload)
                }, {
                hasFetched: true,
                isFetching: false,
                response,
                lastUpdated: meta.receivedAt
            });
		case ActionTypes.LOGIN_FAILURE:
			return Object.assign({}, state, {
                hasError: true,
                error: payload,
                hasFetched: true,
                isFetching: false,
                lastUpdated: meta.receivedAt
            });
		case ActionTypes.LOGIN_REQUEST:
			return Object.assign({}, state, {
                isFetching: true,
                hasError: false,
                hasFetched: false,
                error: null,
                result: {},
                response: {}
            });
		default:
			return state;
	}
}

export default login;