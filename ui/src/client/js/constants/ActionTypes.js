import keyMirror from 'keymirror';

const ActionTypes = keyMirror({

	LOGIN_SUCCESS: null,
	LOGIN_FAILURE: null,
	LOGIN_REQUEST: null,

});

export default ActionTypes;