import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';
import App from './components/App';
import { fetchInitialState } from './actions/ApiActions';
import ensurePolyfills from './utils/ensurePolyfills';

require('../scss/index.scss');

ensurePolyfills(() => {
	const initialState = window.__INITIAL_STATE__; // eslint-disable-line
	const store = configureStore(initialState);

	render(
		<Provider store={store}>
			<App />
		</Provider>,
		document.getElementById('app')
	);

	store.dispatch(fetchInitialState());
});
