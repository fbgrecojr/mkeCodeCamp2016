const ensurePromise = (callback) => {
    if (!window.Promise) {
        require.ensure([], (require) => {
            require('imports?this=>window!es6-promise'); // eslint-disable-line
            callback();
        });
    } else {
        callback();
    }
};

const ensureFetch = (callback) => {
    if (!window.fetch) {
        require.ensure([], (require) => {
            require('isomorphic-fetch'); // eslint-disable-line
            callback();
        });
    } else {
        callback();
    }
};

const ensurePolyfills = (callback) => {
    ensurePromise(() => {
        ensureFetch(callback);
    });
};

export default ensurePolyfills;
