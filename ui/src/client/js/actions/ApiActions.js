import types from '../constants/ActionTypes';

const OPTIONS = {};

export function parseResponse(response) {
    return response.status === 204 ? { json: JSON.stringify({}), response } : response.json().then((json) => ({ json, response })); // provide parsed response and original response
}

export function checkStatus({ json, response }) {
    if (!response.ok) { // status in the range 200-299 or not
        return Promise.reject(new Error(response.statusText || 'Status not OK')); // reject & let catch decide how to handle/throw
    }

    return { json, response };
}

function startAction(type) {
    return { type };
}

export function normalizeJSON({ json, response }) {
    return { json, response };
}

function successAction(type, json, response) {
    return { type, payload: json, response, meta: { receivedAt: Date.now() } };
}

function failureAction(type, error) {
    return { type, payload: error, error: true, meta: { receivedAt: Date.now() } };
}

function makeFetch(url, options) {
    return fetch(url, options || OPTIONS)
        .then(parseResponse)
        .then(normalizeJSON);
}

export function attemptLogin(credentials) {
	return dispatch => {
		dispatch(startAction(types.LOGIN_REQUEST));
        setTimeout( () => {
            makeFetch('http://localhost:8080/api/userservice/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
                body: JSON.stringify(credentials)
            })
                .then(({ json, response }) => dispatch(successAction(types.LOGIN_SUCCESS, json, response)))
                .catch((error) => dispatch(failureAction(types.LOGIN_FAILURE, error)));
        }, 3000);
	}
}

export function fetchInitialState() {

}

