import React, {PropTypes, Component} from 'react';
import {connect} from 'react-redux';
import * as apiActions from '../actions/ApiActions';
import {bindActionCreators} from 'redux';

class Login extends Component {
 	
	constructor(props, context) {
		super(props, context);

		this.state = {
			email: '',
			password: '',
			error: false
		}

		this.handleClick = this.handleClick.bind(this);
		this.onEmailChange = this.onEmailChange.bind(this);
		this.onPasswordChange = this.onPasswordChange.bind(this);
	}

	handleClick(e) {
		e.preventDefault();

		const { email, password } = this.state;

		if(email === '' || password === ''){
			this.setState({error: true});
		} else {
			this.setState({
				error: false
			});
			this.props.actions.attemptLogin({ email, password });
		}
	}

	onEmailChange(e) {
		this.setState({email: e.target.value});
	}

	onPasswordChange(e) {
		this.setState({password: e.target.value});
	}

	render() {

		const { error } = this.state;
		const { login } = this.props;

		return (

			<div className="row padding-medium">
				<div className="small-24 medium-12 large-12 medium-centered large-centered col">
					<div className="card padding-xlarge">
						<form>
							<div className={`${ ( error || (login.response.status === 204) ) ? 'error' : ''} row`}>
								<div className="col">
						            <label>email</label>
						            <input type="text" value={this.state.email} onChange={this.onEmailChange} />
						        </div>
							</div>
							<div className={`${ ( error || (login.response.status === 401) ) ? 'error' : ''} row`}>
								<div className="col">
						            <label>password</label>
						            <input type="password" value={this.state.password} onChange={this.onPasswordChange} />
						        </div>
							</div>
							<div className="row">
								<div className="col">
									<div className={`button ${error || (login.response.status && login.response.status !== 200) ? 'alert' : ''} ${(login.response.status && login.response.status === 200) ? 'success' : ''} expand`} onClick={this.handleClick}>
										<span className={`${login.isFetching ? 'loading-indicator tiny ghost' : 'icon'} ${(login.hasFetched && login.response.status === 200) ? 'icon-unlock' : 'icon-lock'} valign-middle`}></span>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>			
			</div>

		);
	}
}


Login.propTypes = {
	actions: PropTypes.object.isRequired,
	login: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
	return {
		login: state.login
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(apiActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);

