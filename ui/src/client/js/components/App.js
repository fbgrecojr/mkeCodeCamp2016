import React, { PropTypes, Component } from 'react';
import {connect} from 'react-redux';
import Login from './Login';
import * as apiActions from '../actions/ApiActions';
import {bindActionCreators} from 'redux';

class App extends Component {

	render() {
	    return (
			<div className="bg-off-white">
				<main>
					<Login />
				</main>
			</div>
	    );
  	}
}

App.propTypes = {
	actions: PropTypes.object.isRequired,
	login: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
	console.log(state);
	return {
		login: state.login
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(apiActions, dispatch)
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(App);