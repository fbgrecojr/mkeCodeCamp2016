//
// Applies properties pertaining to hiding tooltip.
// 
// @param {Number} $delay	The amount of time to delay the transition.
//
@mixin tooltip-hide($delay: null) {

	visibility: hidden;
	opacity: 0;

	@if ($delay) {
		transition-delay: $delay
	}
}

//
// Applies properties pertaining to showing tooltip.
// 
// @param {Number} $delay	The amount of time to delay the transition.
//
@mixin tooltip-show($delay: null) {

	visibility: visible;
	opacity: 1;

	@if ($delay) {
		transition-delay: $delay;
	}
}

// 
// Computes base tooltip box-shadow properties based on pre-configured values
// of color and offset.
// 
// @param {Color} $color	The color of the shadow
// @param {Number} $offset	A factor off of which the shadow's distance from
// 							the tooltip will be computed.
// 
@mixin tooltip-shadow(
	$color: $tooltip-shadow-color,
	$offset: $tooltip-shadow-offset) {

	$spread: $offset * 0.5;
	$blur: $offset * $tooltip-shadow-blur-multiplier;

	box-shadow: $offset $offset $blur $spread $color;
}

// 
// Computes tooltip box-shadow properties based on pre-configured values of
// color and offset. The side on which the tooltip will sit influences the
// way in which the shadow should display.
// 
// @param {Color} $color	The color of the shadow
// @param {Number} $offset	A factor off of which the shadow's distance from
// 							the tooltip will be computed.
// @param {String} $side	The side on which the tooltip is meant to be
// 							positioned
// 
@mixin tooltip-caret-shadow(
	$color: $tooltip-shadow-color,
	$offset: $tooltip-shadow-offset,
	$side: $tooltip-default-side) {

	$offset-x: $offset * 2.5;
	$offset-y: $offset * 2.5;
	$offset-x-ie: $offset;
	$offset-y-ie: $offset;
	$spread: $offset * -1;
	$blur: $offset * $tooltip-shadow-blur-multiplier;

	@if (($side == "left") or ($side == "bottom")) {
		$offset-y: $offset-y * -1;
		$offset-y-ie: $offset-y-ie * -1;
	}

	@if (($side == "bottom") or ($side == "right")) {
		$offset-x: $offset-x * -1;
		$offset-x-ie: $offset-x-ie * -1;
	}

	box-shadow: $offset-x $offset-y $blur $spread $color;
	@include target-ie10-up() {
		box-shadow: $offset-x-ie $offset-y-ie $blur $spread $color;  
	}
}

// 
// Applies base tooltip properties
// 
@mixin tooltip-base() {
	position: absolute;
	background-color: $tooltip-default-color;
	transition: 0s visibility, 0.2s opacity ease;
	z-index: 1;
	padding: $tooltip-default-padding;
	@include tooltip-shadow();
}

// 
// Applies sizing properties, including display-type and width.
// 
// @param {Number} $width	The desired width.
// 
@mixin tooltip-size(
	$width: $tooltip-width) {

	display: #{if($width == "auto", "inline-block", "block")};
	width: #{$width};
}

// 
// Sets absolute position offset values based on the side on which the
// tooltip should sit.
// 
// @parma {String} $side	The side on which the tooltip should sit.
// 
@mixin tooltip-side(
	$side: $tooltip-default-side) {

	@if ($side == "bottom") {
		bottom: auto;
		top: calc(100% + #{$tooltip-offset});
	}

	@elseif ($side == "top") {
		top: auto;
		bottom: calc(100% + #{$tooltip-offset});
	}

	@elseif ($side == "left") {
		left: auto;
		right: calc(100% + #{$tooltip-offset});
	}

	@elseif ($side == "right") {
		right: auto;
		left: calc(100% + #{$tooltip-offset});
	}
}

// 
// Sets absolute position offset values based on the alignment of the tooltip
// on its desired side.
// 
// @param {String} $alignment	The alignment of the tooltip.
// @param {Number} $width		The current width of the tooltip. If aligned
// 								to middle, specifies negative margin to center.
// 
@mixin tooltip-align(
	$alignment: $tooltip-default-align,
	$width: $tooltip-width) {

	@if ($alignment == "left") {
		left: 0;
		right: auto;
	}

	@elseif ($alignment == "right") {
		right: 0;
		left: auto;
	}

	@elseif ($alignment == "middle") {
		left: 50%;
		right: auto;
		margin-left: $width * -0.5;
	}

	@elseif ($alignment == "top") {
		top: 0;
		bottom: auto;
	}

	@elseif ($alignment == "bottom") {
		bottom: 0;
		top: auto;
	}
}

// 
// Sets base properties for tooltip target. Notably, positions as relative for
// anchoring positioning of `.tooltip` element.
// 
@mixin tooltip-target-base() {
	position: relative;
}

// 
// Sets base properties for tooltip-caret.
// 
@mixin tooltip-caret-base() {
	content: " ";
	position: absolute;
	height: $tooltip-caret-width;
	width: $tooltip-caret-width;
	display: block;
	background-color: $tooltip-default-color;
	transform: rotate(45deg);
	@include tooltip-shadow();
}

// 
// Sets absolute position offset values for the caret based on the side on
// which the tooltip is to sit.
// 
// @param {String} $side	The side on which the tooltip is to sit.
// 
@mixin tooltip-caret-side(
	$side: $tooltip-default-side) {

	@if ($side == "top") {
		top: auto;
		bottom: $tooltip-caret-height * $SQRT2 * -0.5 + 0.15rem;
	}

	@elseif ($side == "bottom") {
		bottom: auto;
		top: $tooltip-caret-height * $SQRT2 * -0.5 + 0.15rem;
	}

	@elseif ($side == "left") {
		left: auto;
		right: $tooltip-caret-height * $SQRT2 * -0.5 + 0.15rem;
	}

	@elseif ($side == "right") {
		right: auto;
		left: $tooltip-caret-height * $SQRT2 * -0.5 + 0.15rem;
	}

	@include tooltip-caret-shadow($side: $side);
}

// 
// Sets absolute position offset values for the tooltip caret based on the
// alignment of the tooltip on its desired side.
// 
// @param {String} $alignment	The alignment of the tooltip.
// 
@mixin tooltip-caret-align(
	$alignment: $tooltip-default-align) {

	@if ($alignment == "right") {
		right: $tooltip-caret-offset;
		left: auto;
		margin-left: 0;
	}

	@elseif ($alignment == "left") {
		left: $tooltip-caret-offset;
		right: auto;
		margin-left: 0;
	}

	@elseif ($alignment == "bottom") {
		bottom: $tooltip-caret-offset;
		top: auto;
		margin-top: 0;
	}

	@elseif ($alignment == "top") {
		top: $tooltip-caret-offset;
		bottom: auto;
		margin-top: 0;
	}	

	@elseif ($alignment == "middle") {
		left: 50%;
		right: auto;
		margin-left: $tooltip-caret-width * -0.5;
	}
}

// 
// Sets background color of tooltip and caret.
// 
// @param {Color} $color	The desired color
// 
@mixin tooltip-color($color) {
	background-color: $color;
	&:before {
		background-color: $color;
	}
}

// 
// Applies base styles to tooltip close button.
// 
@mixin tooltip-close() {
	@include reset-button();

	height: $tooltip-close-font-size;
	width: $tooltip-close-font-size;
	display: block;
	position: absolute;
	top: $tooltip-close-font-size;
	right: $tooltip-close-font-size;
	cursor: pointer;
	opacity: $tooltip-close-opacity;

	&:before {
		@include icon-pseudo-styles($glyph-name: "close");
		font-size: $tooltip-close-font-size;
		margin-top: $tooltip-close-font-size * -0.5;
	}

	&:hover:before { opacity: $tooltip-close-opacity-hover; }
	&:active:before { opacity: $tooltip-close-opacity-click; }
}