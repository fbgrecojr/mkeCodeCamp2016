# UI Toolkit

This UI Toolkit provides LV FEDs with UI tools for rapid development that corresponds with a unified design language.

* View the latest version of the Style Guide app [here](http://uitk.learnvest.com/explorer/latest.html).
* Access the public changelog [here](http://uitk.learnvest.com/explorer).

## Getting Started

### Usage

Use the generated CSS by requesting a static version of a specific theme's ```main.css``` file as served by our CDN.

```html
<link rel="stylesheet" href="//uitk.learnvest.com/v/0.0.5/css/nm/main.css" />
```

The stylesheet `main.css` mentioned here points to CSS from the `nm` theme at version `0.0.5`. Please update your theme according to your needs and use the version that is as up-to-date as possible. Available versions are listed [here](http://uitk.learnvest.com/explorer).

For more information regarding Usage, please see the [_Usage Overview_](./docs/usage/OVERVIEW.md).

### Local Installation

To get the _UI-Toolkit_ up-and-running locally, we recommend the following for quickstart:

```bash
$ npm install
$ npm run init
$ npm run dev
```

For more information regarding Contributing, please see the [_Contributing Overview_](./docs/contributing/OVERVIEW.md).

## Documentation Index

The following articles have been prepared for stakeholders of all varieties. If they are not helpful, feel free to file an issue, as improper documentation can be as disruptive as buggy code.

* **Usage**
	* [Overview](./docs/usage/OVERVIEW.md)
	* [Using Themed CSS](./docs/usage/THEMED_CSS.md)
	* [Importing Sass Variables](./docs/usage/IMPORTING_SASS_VARS.md)
	* [Custom Builds](./docs/usage/CUSTOM_BUILDS.md)
	* [Theming Application-Specific Components](./docs/usage/THEMING_COMPONENTS.md)

* **Contributing**
	* [Installation / Developing Locally](./docs/contributing/OVERVIEW.md)
	* [Sass Architecture Overview](./docs/contributing/METHODOLOGY.md)
	* [Coding Style](./docs/contributing/CODING_STYLE.md)

* **Developer Notes**
	* [Icon Fonts](./docs/dev-notes/ICON_FONTS.md)
	* [Build Pipeline](./docs/dev-notes/BUILD_PIPELINE.md)
	* [Testing](./docs/dev-notes/TESTING.md)
	* [Releases](./docs/dev-notes/RELEASES.md)
